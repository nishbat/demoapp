﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoApp.Models;
using DemoAppApiClient;

namespace DemoApp.Business
{
    public class PetsService : IPetsService
    {
        private readonly IPetsDataService _petsDataService;

        public PetsService(IPetsDataService petsDataService)
        {
            _petsDataService = petsDataService;
        }

        /// <summary>
        /// This method returns the results order by Owner gender descending and Pets name ascending.
        /// It can be made confgurable either by using expression tree or setting with switch case conditionally
        /// </summary>
        /// <returns></returns>
        public List<PetsData> GetPetsAndOwner(string petType)
        {
            if (string.IsNullOrWhiteSpace(petType))
            {
                throw new ArgumentNullException(nameof(petType), "Pet type param is expected");
            }

            var allPets = Task.Run(async () => await _petsDataService.GetPetsData()).Result;

            var filteredPetsData = allPets.Where(p => p.pets != null).Select(parent => new PetsData
            {
                name = parent.name,
                gender = parent.gender,
                age = parent.age,
                pets =
                    parent.pets.Where(child => child.type == petType)
                        .Select(child => new Pet {name = child.name})
                        .OrderBy(n => n.name)
                        .ToList()
            })
                .OrderByDescending(p => p.gender)
                .Where(r => r.pets != null && r.pets.Count > 0)
                .GroupBy(s => s.gender)
                .Select(y => new PetsData { gender = y.Key, pets = y.SelectMany(i => i.pets).OrderBy(j => j.name).ToList()}).ToList();

            return filteredPetsData;
        }
    }
}
