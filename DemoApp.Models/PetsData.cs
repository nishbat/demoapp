﻿using System.Collections.Generic;

namespace DemoApp.Models
{
    /// <summary>
    /// This class is generated using http://json2csharp.com/
    /// </summary>
    public class PetsData
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public List<Pet> pets { get; set; }
    }
}
