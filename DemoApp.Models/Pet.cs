﻿namespace DemoApp.Models
{
    /// <summary>
    /// This class is generated using http://json2csharp.com/
    /// </summary>
    public class Pet
    {
        public string name { get; set; }
        public string type { get; set; }
    }
}
