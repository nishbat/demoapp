﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DemoApp.Models;

namespace DemoAppApiClient
{
    /// <summary>
    /// Data service contract to get pets data from http://agl-developer-test.azurewebsites.net/people.json
    /// </summary>
    public interface IPetsDataService
    {
        /// <summary>
        /// This method gets the pets data using http client
        /// </summary>
        /// <returns>Pets data</returns>
        Task<List<PetsData>> GetPetsData();
    }
}
