﻿using System.Net.Http;
using System.Threading.Tasks;

namespace DemoAppApiClient
{
    public interface IHttpHandler
    {
        Task<HttpResponseMessage> GetAsync(string url);
    }
}
