﻿using System.Web.Mvc;
using DemoApp.Business;

namespace DemoApp.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPetsService _petsService;

        public HomeController(IPetsService petsService)
        {
            _petsService = petsService;
        }

        public ActionResult Index()
        {
            // Currently it is hardcoded to cat but this can be made configurable as well
            var petsData = _petsService.GetPetsAndOwner("Cat");
            return View(petsData);
        }
    }
}